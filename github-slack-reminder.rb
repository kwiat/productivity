#!/usr/bin/env ruby
require 'json'
require 'time'

remind_after_days = 1  # Remind after one day without update of isse
github_user = 'mateuszkwiatkowski'  # Your Github username
github_password = ''  # Your Github password
github_repository = ''  # Github repository to monitor
github_labels = 'devosp'  # Comma separated labels to monitor
slack_nick = 'kwiat'  # Your Slack nick for highlight
slack_webhook = ''  # Notification will be send using this webhook
slack_channel = ''  # Notification will be send to this channel
slack_bot_name = 'Reminder Bot'  # Nickname of the bot on Slack
slack_icon = ':crocodile:'  # Icon displayed near the notification on Slack
zenhub_token = ''  # Token to zenhub account to get pipelines data
zenhub_pipelines = %w('In Progress' 'In Pull Request')  # Inform about tasks in this pipelines

repository = JSON.parse(`curl -s -u #{github_user}:#{github_password} -X GET 'https://api.github.com/repos/#{github_repository}'`)
milestones = JSON.parse(`curl -s -u #{github_user}:#{github_password} -X GET 'https://api.github.com/repos/#{github_repository}/milestones?state=open&sort=due_date&direction=asc'`)
milestone = milestones.first

issues = JSON.parse(`curl -s -u #{github_user}:#{github_password} -X GET 'https://api.github.com/repos/#{github_repository}/issues?state=open&labels=#{github_labels}&milestone=#{milestone['number']}&assignee=#{github_user}'`)

remind_after = 60*60*24
outdated_issues = issues.select { |issue|
  Time.now-Time.parse(issue['updated_at']) >= remind_after*remind_after_days
}

if not outdated_issues.empty?
  outdated_issues.each do |issue|
    zenhub_data = JSON.parse(`curl -s -H 'X-Authentication-Token: #{zenhub_token}' 'https://api.zenhub.io/p1/repositories/#{repository['id']}/issues/#{issue['number']}'`)
    issue['zenhub_pipeline'] = zenhub_data['pipeline']['name']
  end
  slack_msg = ""
  slack_header = "@#{slack_nick}, these in-progress issues from current milestone (#{milestone['title']}) were not updated for more than #{remind_after_days} days:\n"
  outdated_issues.each do |issue|
    slack_msg += "<#{issue['html_url']}|##{issue['number']}> #{issue['title']}\n" if zenhub_pipelines.include?(issue['zenhub_pipeline']['name'])
  end
  if slack_msg.size > 0
    slack_msg = slack_header+slack_msg
    `curl -s -X POST --data-urlencode 'payload={"channel": "#{slack_channel}", "username": "#{slack_bot_name}", "text": "#{slack_msg}", "icon_emoji": "#{slack_icon}"}' #{slack_webhook}`
  end
end
